require('dotenv').config()

const express = require('express'),
    bodyParser = require('body-parser'),
    helmet = require('helmet'),
    conf = require('./config'),
    logger = require('morgan'),
    { ApolloServer, gql } = require('apollo-server-express')

const app = express();

const resolvers = {
    Query: {
        hello: () => 'Hello world!',
    },
};

const typeDefs = gql`
  type Query {
    hello: String
  }
`;


const apollo = new ApolloServer({ typeDefs, resolvers });

apollo.applyMiddleware({ app });

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(helmet())
app.use(logger('dev'))

console.log(apollo.graphqlPath);

app.listen(process.env.PORT || '3001')